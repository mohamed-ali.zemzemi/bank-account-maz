package fr.soat.sg.kata.bankaccountmaz.model;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountTest {

  Account account;

  @BeforeEach
  public void setup() {
    account = new Account(500);
  }

  @Test
  void should_retrieve_right_balance_after_deposit() {
    // GIVEN
    val amount = 200d;

    // WHEN
    account.deposit(amount);

    // THEN
    assertEquals(account.getBalance(), 700);
  }

  @Test
  void should_retrieve_right_balance_after_withdraw() {
    // GIVEN
    val amount = 200d;

    // WHEN
    account.withdraw(amount);

    // THEN
    assertEquals(account.getBalance(), 300);
  }
}
