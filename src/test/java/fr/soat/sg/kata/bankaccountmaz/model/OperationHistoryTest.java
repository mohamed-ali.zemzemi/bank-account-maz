package fr.soat.sg.kata.bankaccountmaz.model;

import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;

class OperationHistoryTest {

  OperationHistory operationHistory;
  Account from;
  Account to;
  Transfer transfer;

  @BeforeEach
  public void setup() {
    from = new Account(1000);
    to = new Account(200);
    transfer = Transfer.builder().from(from).to(to).build();
    operationHistory = OperationHistory.builder().build();
  }

  @Test
  public void should_add_operation() {
    // GIVEN
    val now = now();
    val amount = 100d;
    val operation = new Operation(transfer, now, amount);

    // WHEN
    val result = operationHistory.addOperation(operation);

    // THEN
    assertThat(result.getOperations()).contains(operation);
  }
}
