package fr.soat.sg.kata.bankaccountmaz.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TransferTest {
  Account from;
  Account to;
  Transfer transfer;

  @BeforeEach
  public void setup() {
    from = new Account(500);
    to = new Account(100);
    transfer = new Transfer(from, to);
  }

  @ParameterizedTest
  @CsvSource(value = {"100, 400, 200", "50, 450, 150"})
  void should_transfer_amount(
      final double amount, final double balancePayer, final double balancePayee) {
    // WHEN
    transfer.apply(amount);

    // THEN
    assertEquals(from.getBalance(), balancePayer);
    assertEquals(to.getBalance(), balancePayee);
  }
}
