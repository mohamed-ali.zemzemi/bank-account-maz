package fr.soat.sg.kata.bankaccountmaz.model;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Account {

  @Default double balance = 0;

  public void deposit(final double amount) {
    balance += amount;
  }

  public void withdraw(final double amount) {
    balance -= amount;
  }
}
