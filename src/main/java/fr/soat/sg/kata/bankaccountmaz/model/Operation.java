package fr.soat.sg.kata.bankaccountmaz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Builder
public class Operation {
  Transfer transfer;
  LocalDateTime timestamp;
  double amount;
}
