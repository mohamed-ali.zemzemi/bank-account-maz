package fr.soat.sg.kata.bankaccountmaz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class Transfer {
  Account from;
  Account to;

  public void apply(final double amount) {
    from.withdraw(amount);
    to.deposit(amount);
  }
}
