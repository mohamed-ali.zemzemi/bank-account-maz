package fr.soat.sg.kata.bankaccountmaz.model;

import io.vavr.collection.Set;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;
import lombok.With;

import static io.vavr.API.Set;

@Getter
@Builder
public class OperationHistory {

  @Default @With Set<Operation> operations = Set();

  public OperationHistory addOperation(final Operation operation) {

    withOperations(operations.add(operation));

    return withOperations(operations.add(operation));
  }

  public Set<Operation> getAllOperationFromPayer(final Account payer) {
    return operations.filter(operation -> operation.getTransfer().getFrom().equals(payer));
  }

  public Set<Operation> getAllOperationToPayee(final Account payee) {
    return operations.filter(operation -> operation.getTransfer().getTo().equals(payee));
  }
}
